### Setup ###
Requirements:
- Node 18.14.2 or higher

Setup Instructions:
1. Clone the project from the repository: [https://gitlab.upt.ro/mircea.feder/newgradeu-fe](https://gitlab.upt.ro/mircea.feder/newgradeu-fe)
2. Open a terminal and navigate to the "/fe" folder of the project.
3. Run the command `npm i` to install the project dependencies.
4. After the dependencies are installed, run the command `npm start` to start the frontend application.
